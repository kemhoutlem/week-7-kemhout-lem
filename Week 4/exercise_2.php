<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<style>
    table, th, td {
    border: 1px solid black;
    }
</style>
</head>
<body>
    <?php
        require_once("exercise_2_data_storing/PayByABA.php");
        require_once("exercise_2_data_storing/PayByPiPay.php");
        require_once("exercise_2_data_storing/PayByWing.php");
            
        class PuttingItem{
            private $item;
            private $quantity;
            private $price;
            private $totalPrice;
            public function __construct($item, $quantity, $price){
                $this->item = $item;
                $this->quantity = $quantity;
                $this->price = $price;
                $this->totalPrice = $price*$quantity;
            }
    
            public function getItem(){
                return $this->item;
            }
    
            public function getPrice(){
                return $this->price;
            }
    
            public function getQuantity(){
                return $this->quantity;
            }
    
            public function getTotalPrice(){
                return $this->totalPrice;
            }
        }
        
        class PaymentRecord{
            private $transaction_detail;
            private $transaction_method;

            public function __construct($transaction_detail, $transaction_method){
                $this->transaction_detail = $transaction_detail;
                $this->transaction_method = $transaction_method;
            }

            public function getTransactionDetail(){
                return $this->transaction_detail;
            }
            public function getTransactionMethod(){
                return $this->transaction_method;
            }
        }
        class PaymentMethod{
            private $abaPaymentGateway;
            private $wingPaymentGateway;
            private $pipayPaymentGateway;
            private $paymentRecords = [];

            public function __construct(){
                $this->abaPaymentGateway = new ABA();
                $this->wingPaymentGateway = new Wing();
                $this->pipayPaymentGateway = new PiPaY();
            }

            public function payByABA($transaction){
                $this->abaPaymentGateway->charge($transaction);
                ($this -> paymentRecords)[] = new PaymentRecord($transaction, "ABA");
            }

            public function payByWing($transaction){
                $this -> wingPaymentGateway -> charge($transaction);
                ($this -> paymentRecords)[] =  new PaymentRecord($transaction, "Wing");
            }

            public function payByPipay($transaction){
                $this -> pipayPaymentGateway -> charge($transaction);
                ($this -> paymentRecords)[] =  new PaymentRecord($transaction, "PiPay");          
            }
            
            public function getTotalPaymentRecords(){
                return $this->paymentRecords;
            }

            public function getSortedPaymentRecordsByTotalPrice(){
                $sorted_paymentRecords = $this->paymentRecords;
                usort($sorted_paymentRecords, fn($a, $b) => $a->getTransactionDetail()->getTotalPrice() < $b->getTransactionDetail()->getTotalPrice());
                return $sorted_paymentRecords;
            }


            public function displayPayment($paymentRecords){
                echo "
                    <table>
                    <tr >
                    <th>Name</th>
                    <th>Price</th> 
                    <th>Quantity</th>
                    <th>Methods</th>
                    <th>Total</th>
                    </tr>";
                foreach($paymentRecords as $record){
                    echo "<tr>
                    <td>{$record -> getTransactionDetail() -> getItem()}</td>
                    <td>{$record -> getTransactionDetail() -> getPrice()}</td>
                    <td>{$record -> getTransactionDetail() -> getQuantity()}</td>
                    <td>{$record -> getTransactionMethod()}</td>
                    <td>{$record -> getTransactionDetail() -> getTotalPrice()}</td>
                    </tr>";
                }
            }

            public function getPaymentRecordsByTransactionMethod($transaction_method){
                return array_filter($this->paymentRecords, fn($paymentRecord) => $paymentRecord->getTransactionMethod() == $transaction_method);
            }

            public function getPaymentRecordsByTransactionMethods(...$transaction_methods){
                return array_filter($this->paymentRecords, fn($paymentRecord) => in_array($paymentRecord->getTransactionMethod(),$transaction_methods));
            }

        }
        $item_1 = new PuttingItem("item1", 1, 100);
        $item_2 = new PuttingItem("item2", 2, 300);
        $item_3 = new PuttingItem("item3", 3, 1000);
        $item_4 = new PuttingItem("item4", 4, 50);
        $item_5 = new PuttingItem("item5", 5, 150);
        
        $paymentMethod = new PaymentMethod();

        $paymentMethod->payByWing($item_1);
        $paymentMethod->payByABA($item_2);
        $paymentMethod->payByPipay($item_3);
        $paymentMethod->payByWing($item_4);
        $paymentMethod->payByPipay($item_5);
        
        $paymentMethod -> displayPayment($paymentMethod->getTotalPaymentRecords());
        echo "<div>Total Payment:</div><br>";
        $paymentMethod -> displayPayment($paymentMethod->getPaymentRecordsByTransactionMethod("ABA"));
        echo "<br><div>ABA Payment:</div><br>";
        $paymentMethod -> displayPayment($paymentMethod->getPaymentRecordsByTransactionMethods("PiPay","Wing"));
        echo "<br><div>Wing and Pipay Payment:</div><br>";
        $paymentMethod -> displayPayment($paymentMethod->getSortedPaymentRecordsByTotalPrice());
        echo "<br><div>Sort by Total Price:<div/><br>";
    ?>
</body>
</html>