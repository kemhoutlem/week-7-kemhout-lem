<!DOCTYPE html>
<html lang="en">
    <title> Kemhout</title>
<body>

<?php
    echo "<h2>Advantage: </h2><br>";
    echo "1. class can inherit the functionality of more than one base class.<br>";
    echo "2. subclass has more opportunities to reuse the inherited attributes and operations of the superclasses.<br>";
    echo "<h2>Disadvantage: </h2><br>";
    echo "1. The more superclasses your subclass inherits from, the more maintenance you are likely to perform.<br>";
    echo "2. Some programming languages don’t allow you to use multiple inheritance.<br>";
?>

</body>
</html> 