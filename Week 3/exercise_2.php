<!DOCTYPE html>
<html lang="en">
    <title> Kemhout</title>
<body>

<?php
    $myArray = [2,3,4,6,7,9,11,20,35, 90];

    function filtering_array($myArray) {  
        $arrSize = count($myArray); 
        for ($i=0; $i < $arrSize; $i++) {
            if ($myArray[$i]%2 != 0) {
                unset($myArray[$i]);
            }
        }
        
        foreach ($myArray as $value) {
            echo $value, " ";
        }
    }
    
    $filter_arr = fn($myArray) => filtering_array($myArray);
    $filter_arr($myArray);
?>

</body>
</html> 