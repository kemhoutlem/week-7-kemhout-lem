<!DOCTYPE html>
<html lang="en">
    <title> Kemhout</title>
<body>

<?php
    function sum() {
        $arguments = func_get_args();
        foreach ($arguments as $value){
            $sum += $value;
        }
        return $sum;
    }
    $total1 = sum(1,2,4);
    $total2 = sum(1,2,4,6);
    $total3 = sum(1,2,4,6,10);

    echo $total1;
    echo $total2;
    echo $total3;
?>

</body>
</html> 
